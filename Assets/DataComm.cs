﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

public class DataComm : MonoBehaviour
{
    // Use this for initialization
    private Mesh mesh;
    private List<Vector3> vertices;  // breast vertices
    private List<int> triangles;
    private int[] trianglesUnique;


    public string IP = "192.17.178.183"; //
    public int Port = 25001;
    private byte[] sendData;
    private Socket client;
    private float scale = 1f;
    private float yaw;

    private GameObject tumor;
    private Vector3 tumorPos;
    public Vector3 breastCenter;
    public float arrowOffset = 50f; // height for arrow
    private int start_counter = 0;

    private TcpListener tcpListener;
    private TcpClient connectedTcpClient;

    void Start()
    {
       StartFunc();
       tumor = GameObject.Find("ImageTarget/scan3_extract_from_tetfile/tumor");
       tumorPos = tumor.transform.localPosition;
     //  Prediction(new Vector3(Mathf.Cos(20 * Mathf.Deg2Rad), Mathf.Sin(20 * Mathf.Deg2Rad), 0));
      //  ListenForIncommingRequests();
    }

    private List<Vector3> FlipXVertices(List<Vector3> verts)
    {
        List<Vector3> copy = new List<Vector3>();
        for (int i = 0; i < verts.Count(); i++)
        {
            Vector3 vert = verts[i];
            copy.Add(new Vector3(-vert.x, -vert.y, -vert.z));
        }
        return copy;
    }

    void StartFunc()
    {
        Debug.Log("in start ");
        string verticesFile = "top_processed_unity_node";
        vertices = LoadVertices(verticesFile);

        string upLoad = "Job_6";
        List<Vector3> disps = LoadVertices(upLoad);

        mesh = GetComponent<MeshFilter>().mesh;
        Vector3[] normals = mesh.normals;

        string surfaceFile = "top_processed_unity_face";
        triangles = LoadFaces(surfaceFile);

        List<Vector3> verticesAfterDisp = MoveVertices(vertices.ToList(), disps);
        Debug.Log("verticesAfterDisp " + verticesAfterDisp.Count());

        mesh.vertices = FlipXVertices(verticesAfterDisp.ToList()).ToArray();
        mesh.triangles = triangles.ToArray();
        trianglesUnique = triangles.Distinct().ToArray();
        Debug.Log("trianglesUnique ");
        Array.Sort(trianglesUnique);

        mesh.RecalculateNormals();
        vertices = new List<Vector3>(verticesAfterDisp);
        Debug.Log("finished start ");
    }


    private List<Vector3> LoadVertices(string nodeFile)
    {
        string text = " ";
        Vector3 vertex;
        Debug.Log("Print + " + nodeFile);
        var textFile = Resources.Load<TextAsset>(nodeFile);
        string[] lines = textFile.text.Split('\n');


        List<Vector3> verts = new List<Vector3>();

        for (int i = 0; i < lines.Length; i++)
        {
            string[] arr;
            arr = lines[i].Split(' ');
            vertex = new Vector3(float.Parse(arr[0]), float.Parse(arr[1]), float.Parse(arr[2]));
            verts.Add(vertex);
        }
        return verts;
    }

    private List<int> LoadFaces(string surfaceFile)
    {
        string text = " ";
        Vector3 vertex;
        var textFile = Resources.Load<TextAsset>(surfaceFile);
        string[] lines = textFile.text.Split('\n');

        List<int> triangles = new List<int>();

         for (int i = 0; i < lines.Length; i++)
        {
            string[] arr;
            arr = lines[i].Split(' ');

            triangles.Add(int.Parse(arr[0]));
            triangles.Add(int.Parse(arr[1]));
            triangles.Add(int.Parse(arr[2]));

        }

        return triangles;
    }

    // copy a list of vector3
    public static Vector3[] CopyListVector3(Vector3[] source)
    {
        Vector3[] destination = new Vector3[source.Length];
        for (int i = 0; i < source.Length; i++)
        {
            destination[i] = new Vector3(source[i].x, source[i].y, source[i].z);
        }
        return destination;
    }

    private Vector3 CalculateCenter(List<Vector3> verts)
    {
        Vector3 center = Vector3.zero;
        for (int i = 0; i < verts.Count; i++)
        {
            center += verts[i];
        }
        return center / verts.Count;
    }


    private List<Vector3> BuildVertexMesh(string nodeFile)
    {
        string text = " ";
        Vector3 vertex;
        StreamReader reader = Reader(nodeFile);
        text = reader.ReadLine();
        List<Vector3> verts = new List<Vector3>();
        while (text != null)
        {
            string[] arr;
            arr = text.Split(' ');
            vertex = new Vector3(float.Parse(arr[0]) * scale, float.Parse(arr[1]) * scale, float.Parse(arr[2]) * scale);
            verts.Add(vertex);
            text = reader.ReadLine();//Debug.Log(vertex[0]+" "+vertex[1]+" "+vertex[2]);
        }
        reader.Close();
        return verts;
    }

    private void BuildFaceMesh(string surfaceFile)
    {
    /*    StreamReader reader = Reader(surfaceFile);
        string text = " ";
        text = reader.ReadLine();

        while (text != null)
        {
            string[] arr;
            arr = text.Split(' ');

            triangles.Add(int.Parse(arr[0]));
            triangles.Add(int.Parse(arr[2]));
            triangles.Add(int.Parse(arr[1]));
            text = reader.ReadLine();
        }
        reader.Close(); */
    }

    public static StreamReader Reader(string surfaceFile)
    {
        FileInfo theSourceFile = null;
        StreamReader reader = null;
        theSourceFile = new FileInfo(surfaceFile);
        reader = theSourceFile.OpenText();
        return reader;
    }

    private List<Vector3> MoveVertices(List<Vector3> verts, List<Vector3> disp)
    {
       List<Vector3> verticesAfterDisp = new List<Vector3>();
        for (int i = 0; i < verts.Count; i++)
        {
            verticesAfterDisp.Add(verts[i] + disp[i]);
        }
        return verticesAfterDisp;
    }

    private void PrintMaxDisp(List<Vector3> disp)
    {
     /*   double max = 0;
        int index = 0;
        for (int i = 0; i < disp.Count; i++)
        {
            Vector3 temp = disp[i];
            if (temp.magnitude > max)
            {
                max = temp.magnitude;
                index = i;
            }
        }

      //  Debug.Log(max); */
    }


    private List<Vector3> MoveVerticesThreshold(List<Vector3> vertices, List<Vector3> disp, List<int> indices)
    {
        List<Vector3> verticesAfterDisp = new List<Vector3>(vertices);
        for (int i = 0; i < indices.Count; i++)
        {
            int vertex_index = trianglesUnique[indices[i]];
            verticesAfterDisp[vertex_index] = (vertices[vertex_index] + disp[i]);
        }
        return verticesAfterDisp;
    }

    // Given an object, return its mesh
    public static Mesh ReturnMesh(GameObject obj)
    {
        MeshFilter filter = (MeshFilter)obj.GetComponent("MeshFilter");
        Mesh objMesh = filter.mesh;
        return objMesh;
    }


    public void Prediction(Vector3 direction)
    {
        double timeBeforeTotal = Time.realtimeSinceStartup;
        client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        client.Connect(IP, Port);
        sendData = System.Text.Encoding.ASCII.GetBytes(direction[0].ToString("G4") + "," + direction[1].ToString("G4") + "," + direction[2].ToString("G4"));

        client.Send(sendData);
        byte[] b = ReceiveLargeFile(client, 2093275);

     //   Debug.Log("Time for receiving data: " + (timeAfter - timeBefore).ToString());
        double timeAfterTotal = Time.realtimeSinceStartup;
    //    Debug.Log("Time for sending data1: " + (timeAfterTotal - timeBeforeTotal).ToString());

        string szReceived = System.Text.Encoding.ASCII.GetString(b);
        Vector3 vertex;

    //    Debug.Log("Time for sending data2: " + (Time.realtimeSinceStartup - timeBeforeTotal).ToString());

        List<int> indices = new List<int>();

        List<Vector3> disp;
        if (client.Connected)
        {
            disp = new List<Vector3>();
            string[] words = szReceived.Split(',');

            // the first 3 are tumor

            for (int i = 3; i < words.Length - 4; i += 4)
            {
                int index = int.Parse(words[i]);
                float x = float.Parse(words[i + 1]) * scale;
                float y = float.Parse(words[i + 2]) * scale;
                float z = float.Parse(words[i + 3]) * scale;
                disp.Add(new Vector3(x, y, z));
                indices.Add(index);
            }
            double timeBefore = Time.realtimeSinceStartup;
            double timeAfter = Time.realtimeSinceStartup;


            List<Vector3> verticesAfterDisp = MoveVerticesThreshold(vertices, disp, indices);
            verticesAfterDisp[146386] = (verticesAfterDisp[18063] + verticesAfterDisp[17514] + verticesAfterDisp[17514] + verticesAfterDisp[17516] + verticesAfterDisp[18068] + verticesAfterDisp[18614]) / 6;
            mesh.vertices = FlipXVertices(verticesAfterDisp.ToList()).ToArray();
            tumor.transform.localPosition = tumorPos - new Vector3(float.Parse(words[0]) * scale, float.Parse(words[1]) * scale, float.Parse(words[2]) * scale);
            Debug.Log("tumorPos " + tumorPos);
        }
        else
        {
            Debug.Log(" Not Connected");
        }
       // Debug.Log("Time for sending data3: " + (Time.realtimeSinceStartup - timeBeforeTotal).ToString());
        client.Close();
    }

    public byte[] ReceiveAll(Socket socket)
    {
        var buffer = new List<byte>();
        Debug.Log("socket.Available: " + socket.Available);
        while (socket.Available > 0)
        {
            var currByte = new Byte[1];
            var byteCounter = socket.Receive(currByte, currByte.Length, SocketFlags.None);

            if (byteCounter.Equals(1))
            {
                buffer.Add(currByte[0]);
            }
        }

        return buffer.ToArray();
    }


    private byte[] ReceiveLargeFile(Socket socket, int length)
    {
        byte[] data = new byte[length];

        int size = length;
        var total = 0;
        var dataleft = size;

         while (total < size)
        {
            var recv = socket.Receive(data, total, dataleft, SocketFlags.None);
            if (recv == 0)
            {
                break;
            }
            total += recv;
            dataleft -= recv;
        }
        return data;
    }

    private byte[] ReceiveLargeFile2()
    {
        byte[] bytes;
        using (var mem = new MemoryStream())
        {
            using (var tcp = new TcpClient())
            {
                tcp.Connect(new IPEndPoint(IPAddress.Parse(IP), Port));
                tcp.GetStream().CopyTo(mem);
            }
            bytes = mem.ToArray();
            Debug.Log("bytes size is " + bytes.Length);
        }
        return bytes;
    }

    private void ListenForIncommingRequests () {
        try
        {
          string msg = "";
          // Create listener on port.
          tcpListener = new TcpListener(IPAddress.Parse(IP), Port);
          tcpListener.Start();
          Debug.Log("Server is listening");
          Byte[] bytes = new Byte[1024];
          while (true) {
            using (connectedTcpClient = tcpListener.AcceptTcpClient()) {
              // Get a stream object for reading
              using (NetworkStream stream = connectedTcpClient.GetStream()) {
                int length;
                // Read incomming stream into byte arrary.
                while ((length = stream.Read(bytes, 0, bytes.Length)) != 0) {
                  var incommingData = new byte[length];
                  Array.Copy(bytes, 0, incommingData, 0, length);
                  // Convert byte array to string message.
                  string clientMessage = Encoding.ASCII.GetString(incommingData);
                  msg += clientMessage;
                }
              }
            }
          }
          Debug.Log("msg length: " + msg.Length);
        }
        catch (SocketException socketException)
        {
            Debug.Log("SocketException " + socketException.ToString());
        }
  }

}
