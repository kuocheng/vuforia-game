﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetAndSetText : MonoBehaviour
{

    private float angle;
    private DataComm dataComm;
    private GameObject breast;
    private float x, y, z, scale;
    private int rotatePlaneValue;
    public Slider angleSlider, xSlider, ySlider, zSlider, scaleSlider;

    public Text xValue, yValue, zValue, scaleValue, angleValue;
    private GameObject tumor;

  //  public Text postureAngleValue;
    // Start is called before the first frame update
    void Start()
    {
        breast = GameObject.Find("ImageTarget/scan3_extract_from_tetfile/breast");
        tumor = GameObject.Find("ImageTarget/scan3_extract_from_tetfile/tumor");
        dataComm = breast.GetComponent<DataComm>();
        scale = 1f;
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SetAngle()
    {
        Vector3 direction;

        if (angle == null)
        {
            return;
        }


        if (rotatePlaneValue == 0)
        {
            angle = angle;
            direction = new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), Mathf.Sin(angle * Mathf.Deg2Rad), 0);
        } else
        {
            direction = new Vector3(Mathf.Cos(angle * Mathf.Deg2Rad), 0, Mathf.Sin(angle * Mathf.Deg2Rad));
        }
        Debug.Log("Angle is: " + direction);
        dataComm.Prediction(direction);
    }

    public void SetPosition()
    {
        breast.transform.localPosition += new Vector3(x, y, z);

        Vector3 curPos = breast.transform.localPosition;
        breast.transform.localPosition = Vector3.zero;
        breast.transform.localScale *= scale;
        breast.transform.localPosition = curPos;

        tumor.transform.localPosition += new Vector3(x, y, z);
        curPos = tumor.transform.localPosition;
        tumor.transform.localPosition = Vector3.zero;
        tumor.transform.localScale *= scale;
        tumor.transform.localPosition = curPos;
    }

    public void GetAngle()
    {
        angle = angleSlider.value;
     //   postureAngleValue.text = angle.ToString();
        angleValue.text = angle.ToString();
    }

    public void GetX()
    {
        x = xSlider.value;
        xValue.text = x.ToString();
    }

    public void GetY()
    {
        y = ySlider.value;
        yValue.text = y.ToString();
    }

    public void GetZ()
    {
        z = zSlider.value;
        zValue.text = z.ToString();
    }

    public void GetScale()
    {
        scale = scaleSlider.value;
        scaleValue.text = scale.ToString();
    }

    public void GetRotatePlane(int index)
    {
        Debug.Log("index " + rotatePlaneValue);
        rotatePlaneValue = index;
    }


}
